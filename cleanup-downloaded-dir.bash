#!/bin/bash

dir=$1
if [[ "x${dir}" == "x" ]] ; then
    echo 'ERROR: Must specify a directory.' 1>&2
    exit $(false)
elif [[ ! -d "${dir}" ]] ; then
    echo 'ERROR: Argument must be an existent directory: ' ${dir} 1>&2
    exit $(false)
fi

function cleanfiles() {
    filename=$1
    find . -name ${filename} | xargs rm -rfv | grep --color=auto ${filename}
}

pushd ${dir} &>/dev/null

cleanfiles ".DS_Store"
cleanfiles "__MACOSX"

popd &>/dev/null


